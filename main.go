package main

import (
	"bitbucket.org/playsure-go/zero"
	"fmt"
	"gin-api/config"
	"gin-api/connection"
	"gin-api/routes"
	"gin-api/writer"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"io"
	"log"
	"math/rand"
	"os"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	// Loads env settings.
	viper.SetConfigFile(".env")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		log.Println("Loading env file failed: ", err.Error())
		return
	}

	// Touches database connection.
	db, err := connection.GetDatabase()
	if err != nil {
		log.Println("Database connection failed: ", err.Error())
	} else {
		if len(os.Args) >= 2 && os.Args[1] == "migrate" { // Command for asking database migrations.
			// Do database migration.
			db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci")
			if len(config.Models) > 0 {
				if err2 := db.AutoMigrate(config.Models...); err2 != nil {
					log.Println("Auto migration failed: ", err2.Error())
				} else {
					log.Println("Auto migration completed")
				}
			} else {
				log.Println("Auto migration failed: no target models")
			}
			return
		}
	}

	// Sets env mode.
	appEnv := zero.StringOrDefault(viper.Get("APP_ENV"), config.AppEnv)
	switch appEnv {
	case "production":
		gin.SetMode(gin.ReleaseMode)
	case "local":
	default:
		gin.SetMode(gin.DebugMode)
	}

	// Initializes gin-gonic.
	appName := zero.StringOrDefault(viper.Get("APP_NAME"), config.AppName)
	gin.DisableConsoleColor()
	file := writer.GinLogWriter(appName)
	gin.DefaultWriter = io.MultiWriter(file, os.Stdout)

	router := gin.Default()
	router.Static("/storage", "./storage/public")
	router.SetFuncMap(config.FuncMap)

	// Sets API URL routes.
	routes.RouteApi(router)

	// Starts server.
	serverPort := zero.Uint16OrDefault(viper.Get("SERVER_PORT"), config.ServerPort)
	serverStr := fmt.Sprintf("%s:%d", config.ServerHost, serverPort)

	pem := zero.StringOrZero(viper.Get("TLS_PEM"))
	key := zero.StringOrZero(viper.Get("TLS_KEY"))
	if pem != "" && key != "" { // Uses TLS.
		if err2 := router.RunTLS(serverStr, pem, key); err2 != nil {
			log.Println("Start service failed: ", err2.Error())
		}
	} else {
		if err2 := router.Run(serverStr); err2 != nil {
			log.Println("Start service failed: ", err2.Error())
		}
	}
}
