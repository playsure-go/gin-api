package parameters

type ErrorCode int8

const (
	ERROR_CODE_SUCCESS                  ErrorCode = 0
	ERROR_CODE_FAILURE                  ErrorCode = -1
	ERROR_CODE_PARAMETER_INVALID_FORMAT ErrorCode = -2
	ERROR_CODE_AUTH_FAILURE             ErrorCode = -3
)
