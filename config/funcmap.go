package config

import (
	"gin-api/funcmap"
	"html/template"
)

var FuncMap = template.FuncMap{
	"TimeFormat": funcmap.TimeFormat,
}
