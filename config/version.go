package config

const VersionApi string = "0.0.1"

const (
	VersionName_Android string = "0.0.1"
	VersionCode_Android uint32 = 1
	VersionName_iOS     string = "0.0.1"
	VersionCode_iOS     uint32 = 1
)
