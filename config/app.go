package config

const (
	AppEnv   string = "local"
	AppName  string = "gin-api"
	LogLevel string = "debug"
)
