package config

const (
	DatabaseUsername string = "root"
	DatabasePassword string = ""
	DatabaseHost     string = "127.0.0.1"
	DatabasePort     uint16 = 3306
	DatabaseName     string = "mydb"
)
