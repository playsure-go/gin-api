package logger

import (
	"bitbucket.org/playsure-go/zero"
	"gin-api/config"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var myLogger *logrus.Logger

func initLogger() {
	if myLogger != nil { // myLogger was initialized.
		return
	}

	appName := zero.StringOrDefault(viper.Get("APP_NAME"), config.AppName)
	hook := rotateLogsHook(appName)
	logLevel := getLogLevel()

	myLogger = logrus.New()
	myLogger.SetLevel(logLevel)
	myLogger.SetFormatter(&logrus.JSONFormatter{})
	myLogger.AddHook(hook)
}

func getLogLevel() logrus.Level {
	logLevel, err := logrus.ParseLevel(zero.StringOrDefault(viper.Get("LOG_LEVEL"), config.LogLevel))
	if err != nil {
		logLevel = logrus.DebugLevel
	}
	return logLevel
}

type Fields logrus.Fields

func WithFields(fields Fields) *logrus.Entry {
	initLogger()
	return myLogger.WithFields(logrus.Fields(fields))
}

func Debug(args ...interface{}) {
	initLogger()
	myLogger.Debug(args)
}

func Info(args ...interface{}) {
	initLogger()
	myLogger.Info(args)
}

func Warn(args ...interface{}) {
	initLogger()
	myLogger.Warn(args)
}

func Error(args ...interface{}) {
	initLogger()
	myLogger.Error(args)
}

func Fatal(args ...interface{}) {
	initLogger()
	myLogger.Fatal(args)
}

func Panic(args ...interface{}) {
	initLogger()
	myLogger.Panic(args)
}
