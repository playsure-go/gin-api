package get_version

import "gin-api/parameters"

type Response struct {
	parameters.Response
	Versions *Response_Versions `json:"versions,omitempty"`
}

type Response_Versions struct {
	Api     string                   `json:"api" binding:"required"`
	Ios     Response_Versions_Values `json:"ios" binding:"required"`
	Android Response_Versions_Values `json:"android" binding:"required"`
}

type Response_Versions_Values struct {
	Name string `json:"name" binding:"required"`
	Code uint32 `json:"code" binding:"required"`
}
