package get_version

import (
	"gin-api/config"
	"gin-api/parameters"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetVersion(c *gin.Context) {
	response := &Response{}
	response.ErrorCode = parameters.ERROR_CODE_SUCCESS
	response.Versions = &Response_Versions{}
	response.Versions.Api = config.VersionApi
	response.Versions.Android.Name = config.VersionName_Android
	response.Versions.Android.Code = config.VersionCode_Android
	response.Versions.Ios.Name = config.VersionName_iOS
	response.Versions.Ios.Code = config.VersionCode_iOS
	c.JSON(http.StatusOK, response)
}
