package routes

import (
	"gin-api/controllers/get_version"
	"github.com/gin-gonic/gin"
)

func RouteApi(router *gin.Engine) {
	api := router.Group("/api")
	{
		// router.Use(middleware.Cors())

		// Inserts API routes here.
		api.GET("/version", get_version.GetVersion)
	}
}
