package middleware

import (
	"gascall-odm-api/config"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func Cors() gin.HandlerFunc {
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowMethods = config.CorsAllowMethods
	corsConfig.AllowHeaders = config.CorsAllowHeaders
	return cors.New(corsConfig)
}
