package connection

import (
	"bitbucket.org/playsure-go/zero"
	"fmt"
	"gin-api/config"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var db *gorm.DB

func connectionString() string {
	databaseHost := zero.StringOrDefault(viper.Get("DATABASE_HOST"), config.DatabaseHost)
	databasePort := zero.Uint16OrDefault(viper.Get("DATABASE_PORT"), config.DatabasePort)
	databaseName := zero.StringOrDefault(viper.Get("DATABASE_NAME"), config.DatabaseName)
	databaseUsername := zero.StringOrDefault(viper.Get("DATABASE_USERNAME"), config.DatabaseUsername)
	databasePassword := zero.StringOrDefault(viper.Get("DATABASE_PASSWORD"), config.DatabasePassword)

	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		databaseUsername, databasePassword, databaseHost, databasePort, databaseName)
}

func GetDatabase() (*gorm.DB, error) {
	var err error
	if db == nil {
		db, err = gorm.Open(mysql.Open(connectionString()), &gorm.Config{})
	}
	return db, err
}
